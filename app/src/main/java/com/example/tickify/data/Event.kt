
package com.example.tickify.data

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.example.tickify.R


data class Event(
    @DrawableRes val imageResourceId: Int,
    @StringRes val name: Int,
    @StringRes val fechaE: Int,
    @StringRes val lugar: Int
)

val events = listOf(
    Event(R.drawable.eladio_carrion, R.string.Eladio_CarrionN, R.string.fecha_value, R.string.Eladio_Carrion_Lugar),
    Event(R.drawable.sin_bandera, R.string.Sin_bandera, R.string.fecha_value, R.string.Eladio_Carrion_Lugar),
    Event(R.drawable.set_your, R.string.set_your, R.string.fecha_value, R.string.Eladio_Carrion_Lugar),
)
