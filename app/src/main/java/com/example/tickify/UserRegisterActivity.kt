package com.example.tickify

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.tickify.ui.theme.TickifyTheme


class UserRegisterActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TickifyTheme {
                // Contenedor Surface utilizando el color de fondo del tema
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    UserRegister()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UserRegister(modifier: Modifier = Modifier) {

    var username by remember { mutableStateOf("") }
    var fullName by remember { mutableStateOf("") }
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var confirmPassword by remember { mutableStateOf("") }
    var nameError by remember { mutableStateOf(false) }
    var hidden by remember { mutableStateOf(true) }
    var hidde by remember { mutableStateOf(true) }
    val context = LocalContext.current
    // Columna para el logo
    Box(
        modifier = Modifier
            .fillMaxSize()

    ) {
        Image(
            painter = painterResource(id = R.drawable.juan),
            contentDescription = null, // Puedes proporcionar una descripción si es necesario
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Transparent) // Fondo transparente para evitar espacios en blanco
                .scale(1.15f)


        )

    Column (modifier = modifier.fillMaxSize(), // Toma tanto espacio como sea posible
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top // Alineación en la parte superior
    ){
        Image(
            painter = painterResource(id = R.drawable.logito),
            contentDescription = null,
            modifier = modifier.size(width = 100.dp, height = 100.dp)
        )
        Text(text = "Registrate Gratis",
            color = Color(0xFFF07000),
            style = TextStyle(
                fontWeight = FontWeight.Bold, // Hacer el texto negrita
                fontSize = 24.sp // Ajustar el tamaño del texto

            )
        )


        Spacer(
            modifier = modifier.size(48.dp)
        )


    Column(
        modifier = modifier
            .size(width = 320.dp, height = 420.dp)
            .clip(RoundedCornerShape(32.dp))
            .border(
                2.dp,
                color = Color(0x40F06000),
                RoundedCornerShape(32.dp)
            )
            .background(color = Color.White), // Toma tanto espacio como sea posible
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top // Alineación en la parte superior
    ) {
        Spacer(
            modifier = modifier.size(32.dp)
        )

        TextField(
            value = fullName,
            colors = TextFieldDefaults.textFieldColors(containerColor = Color(0x20F06000),
                focusedLabelColor = Color(0xFFF06000),
                unfocusedLabelColor = Color(0x80F06000),
                focusedIndicatorColor = Color(0x00FFFFFF),
                unfocusedIndicatorColor = Color(0x00FFFFFF)
               ),
            shape = RoundedCornerShape(50),
            onValueChange = { fullName = it },
            label = { Text("Nombre Completo", fontSize = 12.sp) },
            textStyle = TextStyle(fontSize = 12.sp),
            modifier = Modifier
                .height(50.dp)
                .border(1.dp, Color(0xFFFFA500), shape = RoundedCornerShape(50)) // Personaliza el radio aquí

        )

        Spacer(
            modifier = modifier.size(16.dp)
        )

        TextField(
            value = email,

            onValueChange = { email = it },
            isError = nameError,
            label = { Text("Email", fontSize = 12.sp) },
            textStyle = TextStyle(fontSize = 12.sp),

            modifier = Modifier

                .height(50.dp)
                .border(1.dp, Color(0xFFFFA500), shape = RoundedCornerShape(50)) ,// Personaliza el radio aquí
            colors = TextFieldDefaults.run {
                textFieldColors(containerColor = Color(0x20F06000),
                        focusedLabelColor = Color(0xFFF06000),
                        unfocusedLabelColor = Color(0x80F06000),
                        focusedIndicatorColor = Color(0x00FFFFFF),
                        unfocusedIndicatorColor = Color(0x00FFFFFF)
                    )
            },
            shape = RoundedCornerShape(50),
        )

        Spacer(
            modifier = modifier.size(16.dp)
        )

        TextField(
            value = password,
            onValueChange = { password = it },
            label = { Text("Contraseña", fontSize = 12.sp) },
            textStyle = TextStyle(fontSize = 12.sp),
            colors = TextFieldDefaults.textFieldColors(containerColor = Color(0x20F06000),
                focusedLabelColor = Color(0xFFF06000),
                unfocusedLabelColor = Color(0x80F06000),
                focusedIndicatorColor = Color(0x00FFFFFF),
                unfocusedIndicatorColor = Color(0x00FFFFFF)
                ),

            shape = RoundedCornerShape(50),
            visualTransformation =
            if (hidden) PasswordVisualTransformation() else VisualTransformation.None,//3
            trailingIcon = {// 4
                IconButton(onClick = { hidden = !hidden }, modifier = Modifier.size(24.dp)) {
                    val vector = painterResource(//5
                        if (hidden) R.drawable.invisible
                        else R.drawable.ojo
                    )
                    val description = if (hidden) "Ocultar contraseña" else "Revelar contraseña" //6
                    Icon(painter = vector, contentDescription = description)
                }
            },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            modifier = Modifier
                .height(50.dp)
                .border(1.dp, Color(0xFFFFA500), shape = RoundedCornerShape(50)) // Personaliza el radio aquí
        )

        Spacer(
            modifier = modifier.size(16.dp)
        )


        TextField(
            value = confirmPassword,
            colors = TextFieldDefaults.textFieldColors(containerColor = Color(0x20F06000),
                focusedLabelColor = Color(0xFFF06000), 
                unfocusedLabelColor = Color(0x80F06000),
                focusedIndicatorColor = Color(0x00FFFFFF),
                unfocusedIndicatorColor = Color(0x00FFFFFF)
            ),
            shape = RoundedCornerShape(50),
            onValueChange = { confirmPassword = it },
            label = { Text("Confirmar contraseña", fontSize = 12.sp) },
            textStyle = TextStyle(fontSize = 12.sp),
            visualTransformation =
            if (hidde) PasswordVisualTransformation() else VisualTransformation.None,//3
            trailingIcon = {// 4
                IconButton(onClick = { hidde = !hidde }, modifier = Modifier.size(24.dp)) {
                    val vector = painterResource(//5
                        if (hidde) R.drawable.invisible
                        else R.drawable.ojo
                    )
                    val description = if (hidde) "Ocultar contraseña" else "Revelar contraseña" //6
                    Icon(painter = vector, contentDescription = description)
                }
            },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            modifier = Modifier
                .height(50.dp)
                .border(1.dp, Color(0xFFFFA500), shape = RoundedCornerShape(50)) // Personaliza el radio aquí

        )

        Spacer(
            modifier = modifier.size(16.dp)
        )

        Button(onClick = {
            // val intento = Intent(context, UserLoginActivity::class.java)
            // context.startActivity(intento)
        },
            colors =
            ButtonDefaults.buttonColors(containerColor = Color(0xFFF06000))) {
            Text(
                text = "Registrar",
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium,
                color = colorResource(id = R.color.white)
            )
        }
        Spacer(modifier = modifier.size(16.dp))
        Text(text = "¿Ya tienes cuenta?",
            color = Color.Black,
            modifier = Modifier.clickable {
                val intento = Intent(context, UserLoginActivity::class.java)
                context.startActivity(intento)
            }
        )
    }
    }
    }
}



@Preview(showBackground = true)
@Composable
fun UserRegisterPreview() {
    TickifyTheme {
        UserRegister()
    }
}
