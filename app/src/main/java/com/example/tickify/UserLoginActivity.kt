package com.example.tickify

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.tickify.ui.theme.TickifyTheme

class UserLoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TickifyTheme {
                // Contenedor Surface utilizando el color de fondo del tema
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    UserLogin()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UserLogin(modifier: Modifier = Modifier) {

    var username by remember { mutableStateOf("") }
    var fullName by remember { mutableStateOf("") }
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var confirmPassword by remember { mutableStateOf("") }
    var nameError by remember { mutableStateOf(false) }
    var isEmailTextFieldValid by rememberSaveable { mutableStateOf(true) }
    var isPassTextFieldValid by rememberSaveable { mutableStateOf(true) }
    var hidden by remember { mutableStateOf(true) }
    val context = LocalContext.current
    // Columna para el logo
    Box(
        modifier = modifier.fillMaxSize()
    ) {
        Image(
            painter = painterResource(id = R.drawable.juan),
            contentDescription = null, // Puedes proporcionar una descripción si es necesario
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Transparent) // Fondo transparente para evitar espacios en blanco
                .scale(1.15f)


        )
        Column (
            modifier = modifier.fillMaxSize(), // Toma tanto espacio como sea posible
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top // Alineación en la parte superior
        ) {
            // Agregar el logo a la izquierda
            Image(
                painter = painterResource(id = R.drawable.logito),
                contentDescription = null,
                modifier = modifier.size(width = 100.dp, height = 100.dp)
            )
            Text(text = "Accede a tu cuenta",
                color = Color(0xFFF07000),
                style = TextStyle(
                    fontWeight = FontWeight.Bold, // Hacer el texto negrita
                    fontSize = 24.sp // Ajustar el tamaño del texto
                )
            )


            Spacer(
                modifier = modifier.size(48.dp)
            )

            Column(
                modifier = modifier
                    .size(width = 320.dp, height = 420.dp)
                    .clip(RoundedCornerShape(32.dp))
                    .border(
                        2.dp,
                        color = Color(0x40F06000),
                        RoundedCornerShape(32.dp)
                    )
                    .background(color = Color.White), // Toma tanto espacio como sea posible
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top // Alineación en la parte superior
            ) {
                Spacer(
                    modifier = modifier.size(32.dp)
                )
                TextField(
                    value = email,
                    onValueChange = {
                        email = it
                        isEmailTextFieldValid = it.isNotEmpty()
                    },
                    isError = !isEmailTextFieldValid,
                    label = { Text("Email" , fontSize = 12.sp) },
                    colors = TextFieldDefaults.textFieldColors(containerColor = Color(0x20F06000),
                        focusedLabelColor = Color(0xFFF06000),
                        unfocusedLabelColor = Color(0x80F06000),
                        focusedIndicatorColor = Color(0x00FFFFFF),
                        unfocusedIndicatorColor = Color(0x00FFFFFF)
                    ),
                    textStyle = TextStyle(fontSize = 12.sp),
                    modifier = Modifier
                        .height(50.dp)
                        .border(1.dp, Color(0xFFFFA500), shape = RoundedCornerShape(50)) ,// Personaliza el radio aquí

                    shape = RoundedCornerShape(50),
                )

                Spacer(
                    modifier = modifier.size(32.dp)
                )

                TextField(
                    value = password,
                    onValueChange = {
                        password = it
                        isPassTextFieldValid = it.isNotEmpty()
                    },
                    isError = !isPassTextFieldValid,
                    label = { Text("Contraseña" , fontSize = 12.sp) },
                    colors = TextFieldDefaults.textFieldColors(containerColor = Color(0x20F06000),
                        focusedLabelColor = Color(0xFFF06000),
                        unfocusedLabelColor = Color(0x80F06000),
                        focusedIndicatorColor = Color(0x00FFFFFF),
                        unfocusedIndicatorColor = Color(0x00FFFFFF)
                    ),
                    textStyle = TextStyle(fontSize = 12.sp),
                    shape = RoundedCornerShape(50),
                    visualTransformation =
                    if (hidden) PasswordVisualTransformation() else VisualTransformation.None,//3
                    trailingIcon = {// 4
                        IconButton(onClick = { hidden = !hidden }, modifier = Modifier.size(24.dp)) {
                            val vector = painterResource(//5
                                if (hidden) R.drawable.invisible
                                else R.drawable.ojo
                            )
                            val description = if (hidden) "Ocultar contraseña" else "Revelar contraseña" //6
                            Icon(painter = vector, contentDescription = description)
                        }
                    },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    modifier = Modifier
                        .height(50.dp)
                        .border(1.dp, Color(0xFFFFA500), shape = RoundedCornerShape(50)) // Personaliza el radio aquí
                )

                Spacer(
                    modifier = modifier.size(8.dp)
                )

                Text(text = "¿Olvidaste tu contrasena?",
                    style = TextStyle(
                        fontSize = 12.sp,
                        textAlign = TextAlign.Left,
                    ),
                    color = Color.Black,
                    modifier = Modifier.clickable {
                        val intent = Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://developer.android.com/reference/kotlin/java/net/URL"))
                        context.startActivity(intent)
                    }

                )

                Spacer(
                    modifier = modifier.size(96.dp)
                )

                Button(onClick = {
                        if (isEmailTextFieldValid && isPassTextFieldValid) {
                            val intento = Intent(context, HomeActivity::class.java)
                            context.startActivity(intento)
                        }
                    },
                    enabled = password.isNotEmpty() && email.isNotEmpty(),
                    colors =
                    ButtonDefaults.buttonColors(containerColor = Color(0xFFF06000))) {
                    Text(
                        text = "Ingresar",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Medium,
                        color = colorResource(id = R.color.white)
                    )
                }

                Spacer(
                    modifier = modifier.size(16.dp)
                )

                Text(text = "¿No tienes cuenta?",
                    color = Color.Black,
                    modifier = Modifier.clickable {
                        val intento = Intent(context, UserRegisterActivity::class.java)
                        context.startActivity(intento)
                    }
                )
            }
        }
    }
}



@Preview(showBackground = true)
@Composable
fun UserLoginPreview() {
    TickifyTheme {
        UserLogin()
    }
}
