package com.example.tickify

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.tickify.data.Event
import com.example.tickify.data.events
import com.example.tickify.ui.buyticket.BuyTicketActivity
import com.example.tickify.ui.theme.TickifyTheme
import kotlinx.coroutines.delay

class HomeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TickifyTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize()
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.fond4),
                        contentDescription = null, // Puedes proporcionar una descripción si es necesario
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color.Transparent) // Fondo transparente para evitar espacios en blanco
                            .scale(1.25f)

                    )
                    WoofApp()
                }
            }
        }
    }
}

/**
 * Composable that displays an app bar and a list of dogs.
 */
@Composable
fun WoofApp() {

    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        EventTopAppBar()

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp) // Ajusta el espaciado según tus necesidades
        ) {

            OutlinedTextFieldSample(

            )


            IconButton(
                onClick = {
                    // Tu código de manejo de clic aquí
                    println("Botón clickeado")
                },
                modifier = Modifier
                    .align(Alignment.CenterEnd) // Alinea el botón en el centro derecho
                    .padding(2.dp) /// Alinea el botón en el centro derecho
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_filter_list_24dp),
                    contentDescription = stringResource(id = R.string.menu_filter_by_grow_zone)
                )
            }
        }
        TextoPan("Destacado", "Ver Todo",  false,  true)
        // tamaño imagenes para que se ve abien 1200x456
        val imagenesResIds = listOf(
            R.drawable.flayerexample,
            R.drawable.flayer1,
            R.drawable.falyer2,
            R.drawable.flayer3,
            R.drawable.falyer4,
            R.drawable.descarga,
            R.drawable.descarga__1_,
            R.drawable.descarga__2_,
            R.drawable.images,
        )
        ImagenConTituloYBoton(
            imagenesResIds  =imagenesResIds,
            titulo = "CONCIERTOS INTERNACIONALES AHORA  \uD83D\uDD25",
            onBotonClick = {
                val intento = Intent(context, BuyTicketActivity::class.java)
                context.startActivity(intento)
            }
        )
        menuInferior()
        TextoPan("Tendencias", "", false,false)
        FilaDeBotones()

        LazyRow(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)// Esto hace que la lista ocupe el espacio restante en la pantalla
        ) {
            items(events) { evento ->
                StretchableCard(
                    evento = evento,
                    modifier = Modifier.padding(dimensionResource(R.dimen.padding_small))
                )
            }
        }
    }
}


/**
 * Composable that displays a list item containing a dog icon and their information.
 *
 * @param event contains the data that populates the list item
 * @param modifier modifiers to set to this composable
 */

@Composable
fun StretchableCard(
    evento: Event,
    modifier: Modifier = Modifier
) {
    var isExpanded by remember{ mutableStateOf(false) }

    Card(
        modifier = modifier
            .clickable { isExpanded = !isExpanded } // Agrega un clic para expandir/cerrar
            .heightIn(min = 100.dp, max = if (isExpanded) 400.dp else 100.dp) // Altura variable
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(dimensionResource(R.dimen.padding_small))
        ) {
            EventoIcon(evento.imageResourceId)
            EvenInformation(evento.name, evento.lugar, evento.fechaE)

            // Si está expandido, puedes agregar más contenido aquí
            if (isExpanded) {
                Text(
                    text = "Más información",
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}



/**
 * Composable that displays a Top Bar with an icon and text.
 *
 * @param modifier modifiers to set to this composable
 */
@Composable
fun EventTopAppBar(modifier: Modifier = Modifier) {
    var showDialog by remember { mutableStateOf(false) }

    // Función para mostrar el cuadro de diálogo
    val showDialogFunction = {
        showDialog = true
    }

    // Diálogo que muestra "Funcionalidad aún no disponible"
    if (showDialog) {
        AlertDialog(
            onDismissRequest = { showDialog = false },
            title = { Text(text = "Funcionalidad aún no disponible") },
            confirmButton = {
                Button(
                    onClick = { showDialog = false },
                    colors = ButtonDefaults.buttonColors()
                ) {
                    Text(text = "Aceptar")
                }
            }
        )
    }

    // Resto del contenido de la barra superior
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.Top
    ) {
        // Columna izquierda para el logo
        Column(
            modifier = Modifier.wrapContentSize(),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Top
        ) {
            // Agregar el logo a la izquierda
            Image(
                painter = painterResource(id = R.drawable.logo_no_background),
                contentDescription = null,
                modifier = Modifier
                    .size(100.dp)
                    .padding(2.dp)
            )
        }
        Row(
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(modifier = Modifier.height(100.dp))

            // Imagen de la campana con clic
            Image(
                painter = painterResource(id = R.drawable.campana),
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .padding(8.dp)
                    .clickable { showDialogFunction() }
            )

            // Imagen del tuerquita con clic
            Image(
                painter = painterResource(id = R.drawable.engranajes),
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .padding(8.dp)
                    .clickable { showDialogFunction() }
            )
        }
    }
}




/**
 *Composable that displays a photo
 * @param eventIcon is the resource ID for the image of the dog
 * @param modifier modifiers to set to this composable
 */
@Composable
fun EventoIcon(
    @DrawableRes eventIcon: Int,
    modifier: Modifier = Modifier
) { //430x253 tamaño del flayer para que se vea bien :3
    Image(
        modifier = modifier
            .fillMaxWidth()
            .height(dimensionResource(R.dimen.image_size))
            .padding(dimensionResource(R.dimen.padding_small))
            .clip(RectangleShape),
        contentScale = ContentScale.Crop,
        painter = painterResource(eventIcon),
        contentDescription = null
    )
}







/**
 * Composable that displays
 *
 * @param evetName is the resource ID for the string of the
 * @param eventDescrip is the Int that represents
 * @param modifier modifiers to set to this composable
 */
@Composable
fun EvenInformation(
    @StringRes eventName: Int,
    eventLocation: Int,
    eventDescription: Int,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        Text(
            text = stringResource(eventName),
            style = MaterialTheme.typography.displayMedium,
            modifier = Modifier.padding(top = dimensionResource(R.dimen.padding_small)),
            fontSize = 10.sp,
            )
        Text(
            text = stringResource(eventDescription),
            style = MaterialTheme.typography.bodyLarge,
            modifier = Modifier
                .border(
                    width = 1.dp,
                    color = Color(0xFFED7D31),
                    shape = RoundedCornerShape(4.dp)
                )
                .padding(8.dp),
                 fontSize = 10.sp,
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Image(
                painter = painterResource(R.drawable.ubi),
                contentDescription = null,
                modifier = Modifier
                    .size(22.dp)
                    .padding(end = 8.dp)
            )

            Text(
                text = stringResource( eventLocation),
                style = MaterialTheme.typography.bodyLarge,
                fontSize = 10.sp,
            )
        }
    }
}


/**
 * Composable that displays what the UI of the app looks like in light theme in the design tab.
 */

// Texto en pantalla y configuracion de eventos
@Composable
fun TextoPan(textoIzquierda: String, textoDerecha: String, clickableIzquierda: Boolean = false, clickableDerecha: Boolean = false) {
    val showDialogIzquierda = remember { mutableStateOf(false) }
    val showDialogDerecha = remember { mutableStateOf(false) }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = textoIzquierda,
            modifier = if (clickableIzquierda) {
                Modifier
                    .align(Alignment.CenterVertically)
                    .clickable {
                        showDialogIzquierda.value = true
                    }
            } else {
                Modifier.align(Alignment.CenterVertically)
            },
            fontSize = 15.sp
        )
        Text(
            text = textoDerecha,
            modifier = if (clickableDerecha) {
                Modifier
                    .align(Alignment.CenterVertically)
                    .clickable {
                        showDialogDerecha.value = true
                    }
            } else {
                Modifier.align(Alignment.CenterVertically)
            },
            fontSize = 11.sp,
            color = Color(1.0f, 0.5f, 0.0f, 1.0f)
        )
    }

    if (showDialogIzquierda.value) {
        AlertDialog(
            onDismissRequest = { showDialogIzquierda.value = false },
            title = { Text("Opción no disponible - $textoIzquierda") },
            confirmButton = {
                Button(
                    onClick = { showDialogIzquierda.value = false },
                ) {
                    Text(text = "Aceptar")
                }
            }
        )
    }

    if (showDialogDerecha.value) {
        AlertDialog(
            onDismissRequest = { showDialogDerecha.value = false },
            title = { Text("Opción no disponible - $textoDerecha") },
            confirmButton = {
                Button(
                    onClick = { showDialogDerecha.value = false },
                ) {
                    Text(text = "Aceptar")
                }
            }
        )
    }
}

//Barra de Busqueda falta añadirle la funcionalidad para buscar :) jeje
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OutlinedTextFieldSample() {
    var text by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue("", TextRange(0, 20)))
    }

    var showMessageDialog by remember { mutableStateOf(false) }

    OutlinedTextField(
        value = text,
        onValueChange = { newValue ->
            if (newValue.text.length <= 20) {
                text = newValue
            }
        },
        label = { Text("BUSCAR") },
        keyboardActions = KeyboardActions(
            onSearch = {
                // Acción cuando se presiona "Enter"
                showMessageDialog = true
            }

        ),
        //
        modifier = Modifier
            .width(330.dp)
            .padding(1.dp), // Modificador para el espacio y ancho
        shape = RoundedCornerShape(50.dp) // Ajusta el radio aquí
    )

    // Diálogo que muestra "Opción no disponible"
    if (showMessageDialog) {
        AlertDialog(
            onDismissRequest = { showMessageDialog = false },
            title = { Text("Opción no disponible") },
            confirmButton = {
                Button(
                    onClick = { showMessageDialog = false },
                ) {
                    Text(text = "Aceptar")
                }
            }
        )
    }
}



@Composable
fun ImagenConTituloYBoton(
    imagenesResIds: List<Int>, // Lista de recursos de imágenes
    titulo: String,
    onBotonClick: () -> Unit
) {
    var currentImageIndex by remember { mutableStateOf(0) }

    // Comenzar un Coroutine para cambiar automáticamente la imagen cada cierto tiempo
    LaunchedEffect(currentImageIndex) {
        while (true) {
            delay(5000) // Cambiar cada 5 segundos (ajusta según tus necesidades)
            currentImageIndex = (currentImageIndex + 1) % imagenesResIds.size
        }
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
    ) {
        val currentImageResId = imagenesResIds[currentImageIndex]
        val painter = painterResource(id = currentImageResId)

        Image(
            painter = painter,
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .padding(40.dp)
        ) {
            Text(
                text = titulo,
                style = TextStyle(
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                    color = Color.White,
                    textAlign = TextAlign.Start
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 8.dp)
            )

            Button(
                onClick = { onBotonClick() },
                colors = ButtonDefaults.buttonColors(
                    contentColor = Color.White
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
            ) {
                Text(text = "Reservar Ahora")
            }
        }
    }
}
@Composable
fun FilaDeBotones() {
    Row(
        modifier = Modifier
            .fillMaxWidth() // Para ocupar todo el ancho disponible
            .padding(1.dp) // Espaciado alrededor de la fila
    ) {
        Button(
            onClick = { /* Acción para el botón de Arte */ },
            modifier = Modifier
                .weight(1f) // Divide el espacio horizontal igualmente entre los botones
                .padding(horizontal = 2.dp) // Espaciado horizontal entre botones
                .background(Color.Transparent), // Color de fondo del botón
        ) {
            Text(text = "Arte", color = Color.White) // Texto del botón
        }

        Button(
            onClick = { /* Acción para el botón de Música */ },
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 2.dp)
                .background(Color.Transparent),
        ) {
            Text(text = "Música", color = Color.White)
        }

        Button(
            onClick = { /* Acción para el botón de Deporte */ },
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 2.dp)
                .background(Color.Transparent),
        ) {
            Text(text = "Deporte", color = Color.White)
        }
    }
}
@Composable
fun menuInferior()
{
    val context = LocalContext.current

    Row(
        modifier = Modifier
            .fillMaxWidth() // Para ocupar todo el ancho disponible
            .padding(1.dp) // Espaciado alrededor de la fila
    ) {
        TextButton(
            onClick = {
                //val intento = Intent(context, EditProfileActivity::class.java)
                //context.startActivity(intento)
            },
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 1.dp)
                .background(Color.Transparent)
                .clickable { /* Acción para la imagen de Deporte */ }
        ) {
            Image(
                painter = painterResource(R.drawable.hogar), // Reemplaza con el recurso de tu imagen de deporte
                contentDescription = null,
                modifier = Modifier.size(20.dp) // Ajusta el tamaño de la imagen aquí
            )
        }

        TextButton(
            onClick = {
                // val intento = Intent(context, EditProfileActivity::class.java)
                // context.startActivity(intento)
            },
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 1.dp)
                .background(Color.Transparent)
                .clickable { /* Acción para la imagen de Deporte */ }
        ) {
            Image(
                painter = painterResource(R.drawable.calendario_reloj), // Reemplaza con el recurso de tu imagen de deporte
                contentDescription = null,
                modifier = Modifier.size(20.dp) // Ajusta el tamaño de la imagen aquí
            )
        }
        TextButton(
            onClick = {
                // val intento = Intent(context, EditProfileActivity::class.java)
                // context.startActivity(intento)
            },
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 1.dp)
                .background(Color.Transparent)
                .clickable { /* Acción para la imagen de Deporte */ }
        ) {
            Image(
                painter = painterResource(R.drawable.marcador_de_mapa), // Reemplaza con el recurso de tu imagen de deporte
                contentDescription = null,
                modifier = Modifier.size(20.dp) // Ajusta el tamaño de la imagen aquí
            )
        }




        TextButton(
            onClick = {
                val intento = Intent(context, EditProfileActivity::class.java)
                context.startActivity(intento)
            },
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 1.dp)
                .background(Color.Transparent)
                .clickable { /* Acción para la imagen de Deporte */ }
        ) {
            Image(
                painter = painterResource(R.drawable.equipo_de_usuario), // Reemplaza con el recurso de tu imagen de deporte
                contentDescription = null,
                modifier = Modifier.size(20.dp) // Ajusta el tamaño de la imagen aquí
            )
        }


    }
}


@Preview(showBackground = true)
@Composable
fun EventPreview() {
    TickifyTheme(darkTheme = false) {
        WoofApp()
    }
}


@Preview
@Composable
fun EventDarkThemePreview() {
    TickifyTheme(darkTheme = true) {
        WoofApp()
    }
}
