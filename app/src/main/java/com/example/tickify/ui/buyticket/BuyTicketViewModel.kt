package com.example.tickify.ui.buyticket

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

/**
 * ViewModel containing the app data and methods to process the data
 */
class BuyTicketViewModel : ViewModel() {

    private val _uiState = MutableStateFlow(BuyTicketUiState())
    val uiState: StateFlow<BuyTicketUiState> = _uiState.asStateFlow()

    var buyCompleted by mutableStateOf(false)
        private set

    init {
        resetBuy()
    }

    fun resetBuy() {
        _uiState.value = BuyTicketUiState()
        calculateTotal()
        updateBuyCompleted(false)
    }

    fun addTickets() {
        // Normal round in the game
        _uiState.update { currentState ->
            currentState.copy(
                quantity = currentState.quantity.inc(),
            )
        }
        calculateTotal()
    }

    fun removeTickets() {
         if (uiState.value.quantity <= 1)
             return

        // Normal round in the game
        _uiState.update { currentState ->
            currentState.copy(
                quantity = currentState.quantity.dec(),
            )
        }
        calculateTotal()
    }

    private fun calculateTotal() {
        // Normal round in the game
        _uiState.update { currentState ->
            currentState.copy(
                totalCost = currentState.quantity * currentState.cost,
            )
        }
    }

    fun updateBuyCompleted(isCompleted: Boolean) {
        buyCompleted = isCompleted
    }

}
