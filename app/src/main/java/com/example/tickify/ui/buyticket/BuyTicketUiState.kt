package com.example.tickify.ui.buyticket

import java.util.Date

/**
 * Data class that represents the UI state
 */

data class Event (
    val name: String = "Concinerto Internacional",
    val place: String = "Estadio Pascual Guerreo",
    val date: String = "15 de Nov 2023",
    val hour: String = "4:15 PM"
)

data class BuyTicketUiState(
    val event: Event = Event(),
    val category: String = "",
    val cost: Int = 30000,
    val quantity: Int = 1,
    val totalCost: Int = 0,
    val assistantId: Int = 0,
    val status: String = "",
    val createdAt: Date = Date(),
    val updateAt: Date = Date()
)
