package com.example.tickify.ui.buyticket

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.tickify.R
import com.example.tickify.ui.base.Foot
import com.example.tickify.ui.base.Header
import com.example.tickify.ui.base.MyIconButton
import com.example.tickify.ui.theme.TickifyTheme
import java.text.NumberFormat



class BuyTicketActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TickifyTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    BuyTicketScreen()
                }
            }
        }
    }
}




@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BuyTicketScreen(){
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                colors = TopAppBarDefaults.largeTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.background
                ),
                title = {
                    Header(label = stringResource(R.string.comprar_tiquetes))
                },
                actions = {
                    IconButton(onClick = { /* do something */ }) {
                        Icon(
                            imageVector = Icons.Filled.Menu,
                            contentDescription = "Localized description",
                            tint = MaterialTheme.colorScheme.secondary
                        )
                    }
                },
                scrollBehavior = scrollBehavior
            )
        },
        bottomBar = {
            BottomAppBar(
                containerColor = MaterialTheme.colorScheme.background,
            ) {
                Foot()
            }
        }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .verticalScroll(rememberScrollState())
        ) {
            BuyTicketContent()
        }
    }
}

@Composable
fun BuyTicketContent(buyTicketViewModel: BuyTicketViewModel = viewModel()){

    val buyTicketUiState by buyTicketViewModel.uiState.collectAsState()

    Column(
        modifier = Modifier
            .padding(15.dp)
    ) {
        Spacer( modifier = Modifier.size(25.dp))
        TextTitle(stringResource(R.string.title_evento))
        EventCard(
            name = buyTicketUiState.event.name,
            place = buyTicketUiState.event.place,
            date = buyTicketUiState.event.date
        )
        Spacer(modifier = Modifier.size(35.dp))
        TextTitle(text = stringResource(R.string.title_tipo_de_tiquete))
        TicketType()
        Spacer(modifier = Modifier.size(35.dp))
        TextTitle(stringResource(R.string.title_cantidad))
        NumberTickets(
            onClickDec = { buyTicketViewModel.removeTickets() },
            onClickInc = { buyTicketViewModel.addTickets() },
            number = buyTicketUiState.quantity,
            total = buyTicketUiState.totalCost
        )
        Spacer( modifier = Modifier.size(25.dp))
        Button(
            onClick = { buyTicketViewModel.updateBuyCompleted(true) },
            modifier = Modifier.fillMaxWidth()
        ) {
            TextTitle(stringResource(R.string.btn_comprar))
        }
        Spacer(modifier = Modifier.size(10.dp))

        // validar fin de compra
        if (buyTicketViewModel.buyCompleted)
            FinalBuyTicketDialog(
                numberTickets = buyTicketUiState.quantity,
                onConfirmButton = { buyTicketViewModel.resetBuy() }
            )
    }
}


@Composable
fun TextTitle(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        fontWeight = FontWeight.Bold,
        modifier = modifier.padding(bottom = 5.dp)
    )
}

@Composable
fun EventCard(name: String, place: String, date: String, modifier: Modifier = Modifier) {

    val contrast = 1f // 0f..10f (1 should be default)
    val brightness = -100f // -255f..255f (0 should be default)
    val colorMatrix = floatArrayOf(
        contrast, 0f, 0f, 0f, brightness,
        0f, contrast, 0f, 0f, brightness,
        0f, 0f, contrast, 0f, brightness,
        0f, 0f, 0f, 1f, 0f
    )

    ElevatedCard(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
        modifier = modifier
            .padding(top = 10.dp, bottom = 10.dp)
            .height(150.dp)
    ) {
        Box(modifier = Modifier) {
            Image(
                painter = painterResource(id = R.drawable.image8),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxSize(),
                contentScale = ContentScale.Crop,
                //colorFilter = ColorFilter.tint(Color.Gray, blendMode = BlendMode.Darken)
                //colorFilter = ColorFilter.lighting(Color.Gray, Color.Blue)
                colorFilter = ColorFilter.colorMatrix(ColorMatrix(colorMatrix))
                //alpha = 0.5f
            )
            Column (
                verticalArrangement = Arrangement.Center,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(5.dp)
            ) {
                Text(
                    text = name,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier,
                    style = MaterialTheme.typography.headlineSmall,
                    color = Color.White,
                    textAlign = TextAlign.Center
                )
                Text(
                    text = place,
                    modifier = Modifier
                        .padding(1.dp),
                    style = MaterialTheme.typography.bodyMedium,
                    color = Color.White,
                    textAlign = TextAlign.Center
                )
                Text(
                    text = date,
                    modifier = Modifier
                        .padding(1.dp),
                    style = MaterialTheme.typography.bodySmall,
                    color = Color.White,
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}

@Composable
fun TicketType(modifier: Modifier = Modifier) {
    Column {
        Row (
            modifier = Modifier.padding()
        ) {
            Button(
                modifier = Modifier.padding(5.dp),
                onClick = { /*TODO*/ }) {
                Text(text = "VIP")
            }
            OutlinedButton(
                modifier = Modifier.padding(5.dp),
                onClick = { /*TODO*/ }) {
                Text(text = "Ecónomico")
            }
        }
    }
}

@Composable
fun NumberTickets(
    onClickDec: () -> Unit,
    onClickInc: () -> Unit,
    number: Int,
    total: Int,
    modifier: Modifier = Modifier
) {

    Column {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = modifier
                .fillMaxWidth()
                .border(
                    1.dp,
                    MaterialTheme.colorScheme.secondary,
                    shape = RoundedCornerShape(16.dp)
                ),
        ) {
            Spacer(modifier = Modifier.size(10.dp))
            MyIconButton(
                icon = ImageVector.vectorResource(R.drawable.ic_remove),
                description = stringResource(R.string.des_quitar_tiquete),
                tint = MaterialTheme.colorScheme.secondary,
                onClick = onClickDec,
                modifier = Modifier.weight(1f)
            )
            Text(
                text = number.toString(),
                fontWeight = FontWeight.Bold,
                fontSize = 28.sp,
                modifier = Modifier
                    .padding(15.dp)
                    .weight(2f),
                style = TextStyle(textAlign = TextAlign.Center),
            )
            MyIconButton(
                icon = Icons.Filled.Add,
                description = stringResource(R.string.des_agregar_tiquete),
                tint = MaterialTheme.colorScheme.secondary,
                onClick = onClickInc,
                modifier = Modifier.weight(1f)
            )
            Spacer(modifier = Modifier.size(10.dp))
        }
        Spacer(modifier = Modifier.size(25.dp))
        TotalTickets(total)
    }
}

@Composable
fun TotalTickets(total: Int = 0, modifier: Modifier = Modifier) {
    var strTotal = NumberFormat.getCurrencyInstance().format(total)
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .fillMaxWidth()
            .padding(1.dp),
    ) {
        Text(
            text = strTotal,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 5.dp)
        )
    }
}

@Composable
private fun FinalBuyTicketDialog(
    numberTickets: Int,
    onConfirmButton: () -> Unit,
    modifier: Modifier = Modifier
) {
    AlertDialog(
        onDismissRequest = {
            // Dismiss the dialog when the user clicks outside the dialog or on the back
            // button. If you want to disable that functionality, simply use an empty
            // onCloseRequest.
        },
        title = { Text(text = "Compra exitosa!") },
        text = { Text(text = "Compraste $numberTickets entrada (s).") },
        modifier = modifier,
        confirmButton = {
            TextButton(onClick = onConfirmButton) {
                Text(text = "OK")
            }
        }
    )
}


@Preview(showBackground = true)
@Composable
fun BuyTicketPreview() {
    TickifyTheme {
        BuyTicketScreen()
    }
}