package com.example.tickify.ui.base

import android.content.Intent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.example.tickify.EditProfileActivity
import com.example.tickify.HomeActivity
import com.example.tickify.R

@Composable
fun Foot(modifier: Modifier = Modifier) {
    val context = LocalContext.current
    Box(
        modifier = modifier.fillMaxWidth(),
    ) {
        Divider(
            color = MaterialTheme.colorScheme.secondary,
            thickness = 0.5.dp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 2.dp)
        )
        Row(
            modifier = modifier
                .fillMaxWidth()
                .padding(10.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            MyIconButton(
                icon = ImageVector.vectorResource(R.drawable.ic_home_24),
                description = "Home",
                onClick = {
                    val intento = Intent(context, HomeActivity::class.java)
                    context.startActivity(intento)
                }
            )
            Spacer(modifier = Modifier.size(10.dp))
            MyIconButton(
                icon = ImageVector.vectorResource(R.drawable.ic_calendar_month_24),
                description = "Calendar",
                onClick = {/* Here */ }
            )
            Spacer(modifier = Modifier.size(10.dp))
            MyIconButton(
                icon = ImageVector.vectorResource(R.drawable.ic_map_24),
                description = "Map",
                onClick = {/* Here */ }
            )
            Spacer(modifier = Modifier.size(10.dp))
            MyIconButton(
                icon = ImageVector.vectorResource(R.drawable.ic_person_24),
                description = "Map",
                onClick = {
                    val intento = Intent(context, EditProfileActivity::class.java)
                    context.startActivity(intento)
                }
            )
        }
    }
}